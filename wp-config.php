<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'firstblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+oKKdvZ9F%kuQhVaye([P}Ox=Z,9#be!L3h-`)/@5^C{4-%2R;a);VVGf,[Lw+vj');
define('SECURE_AUTH_KEY',  '-R;DSvW|V;$/T.<ESbTuit{jO&Wt/+*RjM~78GC^?*@I>B2uekK}=r_{@6AKEdhc');
define('LOGGED_IN_KEY',    'JtCuLB<_aZvxlE-Y~7]7xp)d^F]l*Q!#i|k%y23|zV%mV+uS~%;[m.T0Bgk*/B/N');
define('NONCE_KEY',        '<8u6 49=Lf3><j&4UH{|/^dYOz-#zmiQ};g8# Z1fPL*Bk[^^(aibk|p1;nji_? ');
define('AUTH_SALT',        'CDc4tp8(2Yl_iL3p8Q^9-_mg9BILTQw$`iB,!R|{sf>~vEu9ACez (#i>EVh01In');
define('SECURE_AUTH_SALT', 'cR@+Q@kb~7Aaf(n~Fvs#h#`@C~Pws,FT/ )Zs)S,YOD>qk!W!4pYZe2Hp_]9qxa9');
define('LOGGED_IN_SALT',   'i`k35f&-l:ci(YH~8.7C<YJOkx|!#U3Q%d5VAcvY1VB/sreCo(Y%0IH nde7]m!L');
define('NONCE_SALT',       'o@d@m>L&#:Z%Gyg!@b{ 6g>2b,z>bk0_|?2`r!r7w_U3pnWwa6PZo_j@:Xm#Xm-z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
