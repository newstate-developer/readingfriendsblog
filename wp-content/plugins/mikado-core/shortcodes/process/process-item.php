<?php
namespace Hoshi\Modules\Shortcodes\Process;

use Hoshi\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessItem implements ShortcodeInterface {
    private $base;

    public function __construct() {
        $this->base = 'mkd_process_item';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                    => esc_html__('Process Item', 'mkd_core'),
            'base'                    => $this->getBase(),
            'as_child'                => array('only' => 'mkd_process_holder'),
            'category' => esc_html__( 'by MIKADO', 'mkd_core' ),
            'icon'                    => 'icon-wpb-process-item extended-custom-icon',
            'show_settings_on_create' => true,
            'params'                  => array(
                array(
                    'type'       => 'attach_image',
                    'heading'    => esc_html__('Image', 'mkd_core'),
                    'param_name' => 'process_image'
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Number', 'mkd_core'),
                    'param_name'  => 'number',
                    'save_always' => true,
                    'admin_label' => true
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Title', 'mkd_core'),
                    'param_name'  => 'title',
                    'save_always' => true,
                    'admin_label' => true
                ),
                array(
                    'type'        => 'textarea',
                    'heading'     => esc_html__('Text', 'mkd_core'),
                    'param_name'  => 'text',
                    'save_always' => true,
                    'admin_label' => true
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Gradient Style on hover', 'mkd_core'),
                    'param_name'  => 'hover_gradient_style',
                    'admin_label' => true,
                    'value'       => array(
                        esc_html__('Yes', 'mkd_core') => 'yes',
                        esc_html__('No', 'mkd_core')  => 'no'
                    ),
                    'save_always' => true,
                    'group'       => esc_html__('Design Options', 'mkd_core'),
                ),
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'process_image'         => '',
            'number'                => '',
            'hover_gradient_style'  => '',
            'title'                 => '',
            'text'                  => '',
        );

        $params = shortcode_atts($default_atts, $atts);

        $params['background_style'] = $this->getBackgroundStyle($params);
        $params['item_classes'] = $this->getItemClasses($params);

        return mikado_core_get_core_shortcode_template_part('templates/process-item-template', 'process', '', $params);
    }

    /**
     * Return Process background style
     *
     * @param $params
     *
     * @return false|string
     */

    private function getBackgroundStyle($params){
        $background_style = array();

        if ($params['process_image']){
            $background_style[] = 'background-image: url('.wp_get_attachment_url($params['process_image']).')';
        }

        return implode('; ', $background_style);
    }

    /**
     * Return Process holder classes
     *
     * @param $params
     *
     * @return false|string
     */

    private function getItemClasses($params){
        $item_classes = array(
            'mkd-process-item-holder'
        );

        if($params['process_image']) {
            $item_classes[] = 'mkd-process-background-image';
        }

        if($params['hover_gradient_style'] === 'yes') {
            $item_classes[] = 'mkd-process-gradient-hover';
        }

        return implode(' ', $item_classes);
    }

}