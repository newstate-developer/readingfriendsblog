<?php
namespace Hoshi\Modules\Shortcodes\SectionTitle;

use Hoshi\Modules\Shortcodes\Lib;

class SectionTitle implements Lib\ShortcodeInterface {
    private $base;

    /**
     * SectionTitle constructor.
     */
    public function __construct() {
        $this->base = 'mkd_section_title';

        add_action('vc_before_init', array($this, 'vcMap'));
    }


    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Section Title', 'mkd_core'),
            'base'                      => $this->base,
            'category' => esc_html__( 'by MIKADO', 'mkd_core' ),
            'icon'                      => 'icon-wpb-section-title extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Title', 'mkd_core'),
                    'param_name'  => 'title',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Enter title text', 'mkd_core')
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__('Color', 'mkd_core'),
                    'param_name'  => 'title_color',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Choose color of your title', 'mkd_core')
                ),
                array(
                    'type'			=> 'textfield',
                    'heading'		=> esc_html__('Highlighted Title Text','mkd_core'),
                    'param_name'	=> 'highlighted_text',
                    'value'			=> '',
                    'admin_label'	=> true,
                    'description'   =>esc_html__('Highlighted title text will be appended to title text','mkd_core'),
                    'dependency'  => array('element' => 'type_out', 'value' => array('no'))
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__('Highlighted text color', 'mkd_core'),
                    'param_name'  => 'highlighted_color',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true,
                    'dependency'  => array('element' => 'type_out', 'value' => array('no')),
                    'description' => esc_html__('Choose color of highlighted text', 'mkd_core')
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Text Transform', 'mkd_core'),
                    'param_name'  => 'title_text_transform',
                    'value'       => array_flip(hoshi_mikado_get_text_transform_array(true)),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Choose text transform for title', 'mkd_core')
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Text Align', 'mkd_core'),
                    'param_name'  => 'title_text_align',
                    'value'       => array(
                        ''                          => '',
                        esc_html__('Center', 'mkd_core') => 'center',
                        esc_html__('Left', 'mkd_core')   => 'left',
                        esc_html__('Right', 'mkd_core')  => 'right'
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Choose text align for title', 'mkd_core')
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Margin Bottom', 'mkd_core'),
                    'param_name'  => 'margin_bottom',
                    'value'       => '',
                    'save_always' => true,
                    'admin_label' => true,
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Size', 'mkd_core'),
                    'param_name'  => 'title_size',
                    'value'       => array(
                        esc_html__('Default', 'mkd_core') => '',
                        esc_html__('Small', 'mkd_core')   => 'small',
                        esc_html__('Medium', 'mkd_core')  => 'medium',
                        esc_html__('Large', 'mkd_core')   => 'large'
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Choose one of predefined title sizes', 'mkd_core')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'type_out',
                    'heading'     => esc_html__('Enable Type Out Effect', 'mkd_core'),
                    'value'       => array(
                        esc_html__('No', 'mkd_core')  => 'no',
                        esc_html__('Yes', 'mkd_core') => 'yes',
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'description' => esc_html__('If set to Yes, you can enter two more Section Titles to follow the original one in a typing-like effect.', 'mkd_core')
                ),
                array(
                    'type'        => 'textfield',
                    'param_name'  => 'title_2',
                    'heading'     => esc_html__('Section Title 2', 'mkd_core'),
                    'admin_label' => true,
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'dependency'  => array('element' => 'type_out', 'value' => array('yes'))
                ),
                array(
                    'type'        => 'textfield',
                    'param_name'  => 'title_3',
                    'heading'     => esc_html__('Section Title 3', 'mkd_core'),
                    'admin_label' => true,
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'dependency'  => array('element' => 'title_2', 'not_empty' => true)
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'loop',
                    'heading'     => esc_html__('Loop Titles', 'mkd_core'),
                    'value'       => array(
                        esc_html__('No', 'mkd_core')  => 'no',
                        esc_html__('Yes', 'mkd_core') => 'yes',
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'dependency'  => array('element' => 'type_out', 'value' => array('yes'))
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'cursor_style',
                    'heading'     => esc_html__('Cursor Style', 'mkd_core'),
                    'value'       => array(
                        esc_html__('Gradient', 'mkd_core')    => 'gradient',
                        esc_html__('Solid Color', 'mkd_core') => 'solid_color',
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'dependency'  => array('element' => 'type_out', 'value' => array('yes'))
                ),
                array(
                    'type'        => 'colorpicker',
                    'admin_label' => true,
                    'heading'     => esc_html__('Cursor Color Style', 'mkd_core'),
                    'param_name'  => 'cursor_color_style',
                    'save_always' => true,
                    'description' => '',
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'dependency'  => array('element' => 'cursor_style', 'value' => array('solid_color'))
                ),
                array(
                    'type'        => 'dropdown',
                    'admin_label' => true,
                    'heading'     => esc_html__('Cursor Gradient Style', 'mkd_core'),
                    'param_name'  => 'cursor_gradient_style',
                    'value'       => array_flip(hoshi_mikado_get_gradient_bottom_to_top_styles('-text')),
                    'save_always' => true,
                    'description' => '',
                    'group'       => esc_html__('TypeOut Options', 'mkd_core'),
                    'dependency'  => array('element' => 'cursor_style', 'value' => array('gradient'))
                ),
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'title'                 => '',
            'title_color'           => '',
            'highlighted_text'      => '',
            'highlighted_color'      => '',
            'title_size'            => '',
            'title_text_transform'  => '',
            'title_text_align'      => '',
            'margin_bottom'         => '',
            'type_out'              => '',
            'title_2'               => '',
            'title_3'               => '',
            'loop'                  => '',
            'cursor_style'          => '',
            'cursor_gradient_style' => '',
            'cursor_color_style'    => '',
        );

        $params = shortcode_atts($default_atts, $atts);

        if($params['title'] !== '') {
            $params['section_title_classes'] = array('mkd-section-title');

            if($params['title_size'] !== '') {
                $params['section_title_classes'][] = 'mkd-section-title-'.$params['title_size'];
            }

            $params['section_title_styles'] = array();

            if($params['title_color'] !== '') {
                $params['section_title_styles'][] = 'color: '.$params['title_color'];
            }

            if($params['title_text_transform'] !== '') {
                $params['section_title_styles'][] = 'text-transform: '.$params['title_text_transform'];
            }

            if($params['title_text_align'] !== '') {
                $params['section_title_styles'][] = 'text-align: '.$params['title_text_align'];
            }

            if($params['margin_bottom'] !== '') {
                $params['section_title_styles'][] = 'margin-bottom: '.hoshi_mikado_filter_px($params['margin_bottom']).'px';
            }

            $params['title_tag']     = $this->getTitleTag($params);
            $params['type_out_data'] = $this->getTypeOutData($params);
            $params['highlighted_style'] = $this->getTitleHighlightedStyle($params);

            return mikado_core_get_core_shortcode_template_part('templates/section-title-template', 'section-title', '', $params);
        }
    }

    private function getTitleTag($params) {
        switch($params['title_size']) {
            default:
                $titleTag = 'h2';
        }

        return $titleTag;
    }

    /**
     * Generates style for title highlighted text
     *
     * @param $params
     *
     * @return string
     */
    private function getTitleHighlightedStyle($params){
        $highlighted_style = array();

        if ($params['highlighted_color'] != ''){
            $highlighted_style[] = 'color: '.$params['highlighted_color'];
        }

        return implode(';', $highlighted_style);
    }

    /**
     * Return Type Out data
     *
     * @param $params
     *
     * @return string
     */
    private function getTypeOutData($params) {
        $type_out_data = array();

        if(!empty($params['loop'])) {
            $type_out_data['data-loop'] = $params['loop'];
        }

        if(!empty($params['cursor_gradient_style'])) {
            $type_out_data['data-cursor-gradient'] = $params['cursor_gradient_style'];
        }

        if(!empty($params['cursor_color_style'])) {
            $type_out_data['data-cursor-color'] = $params['cursor_color_style'];
        }

        return $type_out_data;
    }
}