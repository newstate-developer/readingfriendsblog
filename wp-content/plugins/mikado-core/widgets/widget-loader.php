<?php

if(!function_exists('hoshi_mikado_register_widgets')) {

	function hoshi_mikado_register_widgets() {

		$widgets = array(
			'HoshiMikadoLatestPosts',
			'HoshiMikadoSearchOpener',
			'HoshiMikadoSideAreaOpener',
			'HoshiMikadoStickySidebar',
			'HoshiMikadoSocialIconWidget',
			'HoshiMikadoSeparatorWidget',
			'HoshiMikadoCallToActionButton',
			'HoshiMikadoHtmlWidget',
			'HoshiMikadoPostCategories'
		);

		if( hoshi_mikado_is_plugin_installed('woocommerce') ) {
			$widgets[] = 'HoshiMikadoWoocommerceDropdownCart';
		}

		if( hoshi_mikado_is_plugin_installed('contact-form-7') ) {
			$widgets[] = 'HoshiMikadoContactForm7';
		}

		foreach($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'hoshi_mikado_register_widgets');