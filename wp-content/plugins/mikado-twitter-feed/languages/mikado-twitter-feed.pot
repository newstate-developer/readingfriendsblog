# Copyright (C) 2019 Mikado Themes
# This file is distributed under the same license as the Mikado Twitter Feed plugin.
msgid ""
msgstr ""
"Project-Id-Version: Mikado Twitter Feed 1.0.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/mikado-twitter-feed\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-10-25T15:03:29+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.3.0\n"
"X-Domain: mikado-twitter-feed\n"

#. Plugin Name of the plugin
msgid "Mikado Twitter Feed"
msgstr ""

#. Description of the plugin
msgid "Plugin that adds Twitter feed functionality to our theme"
msgstr ""

#. Author of the plugin
msgid "Mikado Themes"
msgstr ""

#: lib/mkd-twitter-api.php:243
#: lib/mkd-twitter-api.php:311
#: lib/mkd-twitter-api.php:378
msgid "Internal WP error"
msgstr ""

#: lib/mkd-twitter-api.php:260
msgid "Redirect URL couldn't be generated"
msgstr ""

#: lib/mkd-twitter-api.php:263
msgid "Ok"
msgstr ""

#: lib/mkd-twitter-api.php:267
msgid "Couldn't connect with Twitter API"
msgstr ""

#: lib/mkd-twitter-api.php:324
msgid "Access token obtained"
msgstr ""

#: lib/mkd-twitter-api.php:329
msgid "Authorize token and it's secret were not obtained"
msgstr ""

#: lib/mkd-twitter-api.php:388
msgid "Couldn't connect with Twitter"
msgstr ""

#: lib/mkd-twitter-api.php:393
msgid "It seems like you haven't connected with your Twitter account"
msgstr ""

#: lib/mkd-twitter-api.php:403
msgid "Couldn't retrieve content from database"
msgstr ""

#: widgets/mkd-twitter-widget.php:12
msgid "Mikado Twitter Widget"
msgstr ""

#: widgets/mkd-twitter-widget.php:13
msgid "Display tweets"
msgstr ""

#: widgets/mkd-twitter-widget.php:24
msgid "Title"
msgstr ""

#: widgets/mkd-twitter-widget.php:29
msgid "User ID"
msgstr ""

#: widgets/mkd-twitter-widget.php:34
msgid "Number of tweets"
msgstr ""

#: widgets/mkd-twitter-widget.php:39
msgid "Show tweet time"
msgstr ""

#: widgets/mkd-twitter-widget.php:41
msgid "No"
msgstr ""

#: widgets/mkd-twitter-widget.php:42
msgid "Yes"
msgstr ""

#: widgets/mkd-twitter-widget.php:48
msgid "Tweets Cache Time"
msgstr ""

#: widgets/mkd-twitter-widget.php:156
msgid "It seems that you haven't connected with your Twitter account"
msgstr ""
