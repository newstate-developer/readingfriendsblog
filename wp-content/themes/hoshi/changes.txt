1.9
- Added WooCommerce 3.7.1 compatibility
- Updated WPBakery Page Builder to 6.0.5
- Updated Revolution Slider to 6.1.3
- Updated Mikado Core to 1.0.4
- Updated Mikado Instagram Feed plugin to 1.0.2
- Updated Mikado Twitter Feed plugin to 1.0.2
- Fixed Hover box button functionality
- Improved Fullscreen header functionality
- Removed unused file instagram-redirect.php from Mikado Instagram Feed plugin
- Removed unused file twitter-redirect.php from Mikado Twitter Feed plugin
- Minor changes and fixes

1.8
- Added WooCommerce 3.6.4 compatibility
- Updated WPBakery Page Builder to 6.0.3
- Fixed per page options style
- Fixed multiple upload warning for portfolio items
- Improved displaying content on secured protocol
- Improved WooCommerce Dropdown Cart widget

1.7
- Added WooCommerce 3.6.1 compatibility
- Minor changes and fixes

1.6
- Added WooCommerce 3.5.7 compatibility
- Updated WPBakery Page Builder to 5.7
- Updated Revolution Slider to 5.4.8.3
- Updated Mikado Core to 1.0.3
- Updated Mikado Instagram Feed to 1.0.1
- Updated Mikado Twitter Feed to 1.0.1
- Improved framework files

1.5
- Added WooCommerce 3.4.5 compatibility
- Added Gutenberg compatibility
- Updated WPBakery Page Builder to 5.5.4
- Updated Revolution Slider to 5.4.8
- Fixed displaying media items on secured protocol

1.4
- Added WooCommerce 3.3.5 compatibility
- Updated WPBakery Page Builder to 5.4.7
- Updated Revolution Slider to 5.4.7.3.
- Fixed deprecated create_function

1.3
- Added WooCommerce 3.2.5 compatibility
- Added Envato Market plugin as required
- Added recommended plugins (WooCommerce and Contact Form 7)
- Updated WPBakery Page Builder to 5.4.5
- Updated Revolution Slider to 5.4.6.4
- Updated Mikado Core plugin to 1.0.2
- Improved menu import for PHP 7.1
- Improved import for Revolution slider slides

1.2
- Added WooCommerce 3.1.1 compatibility
- Fixed potential security issue when saving theme options

1.1
- Added WooCommerce 3.1 compatibility
- Updated Visual Composer to 5.2
- Updated Revolution Slider to 5.4.5.1
- Updated Mikado Core plugin to 1.0.1
